import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_7/widget/button_widget.dart';

class TambahLokasi extends StatefulWidget{
  @override
  _tambahLokasi createState() => _tambahLokasi();
  }

  class _tambahLokasi extends State<TambahLokasi>{
    final formKey = GlobalKey<FormState>();
    String namaLokasi = '';
    String provinsiLokasi = '';
    String alamatLokasi = '';
  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      title: Text("Tambah Lokasi"),
      backgroundColor: Colors.indigo,
    ),
    body: Form(
      key: formKey,
      child: ListView(
        padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
        children: [
          inputNama(),
          const SizedBox(height: 16),
          inputProvinsi(),
          const SizedBox(height: 16),
          inputAlamat(),
          const SizedBox(height: 16),
          inputTombol(),
        ],
      ),
    ),
  );
  Widget inputNama() => TextFormField(
    decoration: InputDecoration(
      labelText: 'Nama Lokasi',
      border: OutlineInputBorder(),
    ),
    validator: (value){
      if(value!.length <=0){
        return 'Enter anything';
      }
      else{
        return null;
      }
    },
    maxLength: 30,
    onSaved: (value)=>setState(()=> namaLokasi = value!),
  );

    Widget inputProvinsi() => TextFormField(
      decoration: InputDecoration(
        labelText: 'Provinsi Lokasi',
        border: OutlineInputBorder(),
      ),
      validator: (value){
        if(value!.length <=0){
          return 'Enter anything';
        }
        else{
          return null;
        }
    },
        maxLength: 30,
        onSaved: (value)=>setState(()=> provinsiLokasi = value!),
    );

    Widget inputAlamat() => TextFormField(
      decoration: InputDecoration(
        labelText: 'Alamat Lokasi',
        border: OutlineInputBorder(),
      ),
      validator: (value){
        if(value!.length <=0){
          return 'Enter anything';
        }
        else{
          return null;
        }
      },
      maxLength: 30,
      onSaved: (value)=>setState(()=> alamatLokasi = value!),
    );

    Widget inputTombol() => IconButton(
        onPressed: (){
          final isValid = formKey.currentState!.validate();
          if(isValid) {
            formKey.currentState!.save();
            final message = 'Berhasil menambahkan lokasi: \n Nama Lokasi: $namaLokasi \n Provinsi Lokasi: $provinsiLokasi \n Alamat Lokasi: $alamatLokasi';
            final wind = SnackBar(
              content: Text(
                message,
                style: TextStyle(fontSize: 20),
              ),
              backgroundColor: Colors.amber,);
            ScaffoldMessenger.of(context).showSnackBar(wind);
          }
        }, icon: Icon(Icons.circle),
        );
  }
