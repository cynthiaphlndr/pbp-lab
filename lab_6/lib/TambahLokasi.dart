import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TambahLokasi extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah Lokasi"),
        backgroundColor: Colors.indigo,
      ),
      body: Padding(
        padding: const EdgeInsets.all(30),
        child: ListView(
          children: [
            Container(
              padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
              child: const Text('Tambah Lokasi Baru',
              style: TextStyle(
                fontSize: 30, fontWeight: FontWeight.bold,
              )),
            ),
          Container(
            padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
            child: const Text('Nama Lokasi',
            style: TextStyle(
              fontSize: 20,
            ))
          ),
          Container(
            height: 50,
            padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
            child: const TextField(
              decoration:  InputDecoration(
                border: OutlineInputBorder(),
              ),
            ),
          ),
            Container(
                padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
                child: const Text('Provinsi Lokasi',
                    style: TextStyle(
                      fontSize: 20,
                    ))
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
              child: const TextField(
                decoration:  InputDecoration(
                  border: OutlineInputBorder(),
                ),
              ),
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
                child: const Text('Alamat Lokasi',
                    style: TextStyle(
                      fontSize: 20,
                    ))
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
              child: const TextField(
                decoration:  InputDecoration(
                  border: OutlineInputBorder(),
                ),
              ),
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
                child: const Text('Jam Buka',
                    style: TextStyle(
                      fontSize: 20,
                    ))
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
              child: const TextField(
                decoration:  InputDecoration(
                  border: OutlineInputBorder(),
                ),
              ),
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
                child: const Text('Jam Tutup',
                    style: TextStyle(
                      fontSize: 20,
                    ))
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
              child: const TextField(
                decoration:  InputDecoration(
                  border: OutlineInputBorder(),
                ),
              ),
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
                child: const Text('Nomor Telepon',
                    style: TextStyle(
                      fontSize: 20,
                    ))
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
              child: const TextField(
                decoration:  InputDecoration(
                  border: OutlineInputBorder(),
                ),
              ),
            ),
            Container(
                alignment: Alignment.center,
                height: 50,
                padding: const EdgeInsets.fromLTRB(100, 10, 150, 10),
                child: ElevatedButton(
                    child: const Text('Tambah Lokasi'),
                    onPressed: () {Navigator.pop(context);
                    },
                    style:ElevatedButton.styleFrom(
                        primary: Colors.indigo,
                        textStyle: const TextStyle(
                          color: Colors.white,)))),
          ],
        ),
      ),
    );
  }
}