import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_6/TambahLokasi.dart';

void main() {
  runApp(Location()
  );
}



class Location extends StatelessWidget {
  const Location({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Slowlab',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.amber,
        fontFamily: 'Times New Roman',
        primarySwatch: Colors.blue,
      ),
      home: const MyApp(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key, required this.title}) : super(key: key);


  final String title;

  @override
  State<MyApp> createState() => _State();
}

class _State extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Slowlab"),
        backgroundColor: Colors.indigo,
      ),
      body: Padding(
        padding: const EdgeInsets.all(30),
        child: ListView(
          children: [
            Container(
              alignment: Alignment.center,
              child: const Text("Cari Lokasi Tes Covid-19",
                  style: TextStyle(
                    fontSize: 30, fontWeight: FontWeight.bold,
                  )),
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.fromLTRB(100, 10, 150, 10),
              height: 50,
              child: const TextField(
                decoration:  InputDecoration(
                  border: OutlineInputBorder(),
                ),
              ),
            ),
            Container(
            alignment: Alignment.center,
            height: 50,
            padding: const EdgeInsets.fromLTRB(100, 10, 150, 10),
            child: ElevatedButton(
              child: const Text('Cari'),
              onPressed: () {
              },
              style:ElevatedButton.styleFrom(
              primary: Colors.indigo,
              textStyle: const TextStyle(
              color: Colors.white,
              )))),
            Container(
              padding: const EdgeInsets.fromLTRB(0, 30, 0, 10),
                child: const Text("Slowlab Hadir di Berbagai Daerah Untuk Memudahkan Anda!",
                style: TextStyle(
                  fontSize: 25,
                )),
              alignment: Alignment.center,
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: const Text("Lab Pusat",
                style: TextStyle(
                  fontSize: 20,
                )),
                alignment:Alignment.center,
            ),
            Center(
              child: Card(
                color: Colors.indigo,
                child: Container(
                  padding: const EdgeInsets.fromLTRB(0, 30, 0, 10),
                  alignment: Alignment.center,
                  width: 200,
                  child: Column(
                    children: [ Container( child: Text(
                      'Slowlab Jakarta Pusat', style: TextStyle(backgroundColor: Colors.amber, color: Colors.indigo, ),
                    )),
                      Container( padding: const EdgeInsets.fromLTRB(7, 20, 7, 10), alignment: Alignment.centerLeft, child: Text('Alamat : Jl. Restu, Kec. Kemayoran, Jakarta Pusat, 10640', style: TextStyle(height: 1),
                    )),
                  Container( padding: const EdgeInsets.fromLTRB(7, 10, 7, 10), alignment: Alignment.centerLeft, child: Text("Jam Operasional : 10.00 - 18.00", style: TextStyle(height: 1))), Container(padding: const EdgeInsets.fromLTRB(7, 10, 7, 10), alignment: Alignment.centerLeft, child:Text('No. Telepon : (021)6711291', style: TextStyle(height: 1))),],
                   )),
                    ),
              ),
            Container(
              alignment: Alignment.center,
                height: 50,
                padding: const EdgeInsets.fromLTRB(100, 10, 150, 10),
                child: ElevatedButton(
                    child: const Text('Tambah Lokasi'),
                    onPressed: () { Navigator.push(context, MaterialPageRoute(builder: (context)=> TambahLokasi()));
                    },
                    style:ElevatedButton.styleFrom(
                        primary: Colors.amber,
                        textStyle: const TextStyle(
                          color: Colors.indigo,
                        )))),
          ],
        )

      ),
    );
  }
}
