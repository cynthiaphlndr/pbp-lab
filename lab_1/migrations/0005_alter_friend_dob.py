# Generated by Django 3.2.7 on 2021-09-18 07:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0004_alter_friend_dob'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friend',
            name='DOB',
            field=models.DateField(),
        ),
    ]
