from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers


# Create your views here.
from datetime import datetime, date
from .models import Note

mhs_name = 'Cynthia Philander'  # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 2, 21)  # TODO Implement this, format (Year, Month, Date)
npm = 2006597292  # TODO Implement this


def index(request):
    notes = Note.objects.all().values()
    response = {'notes':notes}
    return render(request, 'lab2.html', response)

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")


