1. Apakah perbedaan antara JSON dan XML?
JSON: Bahasa pemrograman berdasarkan Javascript. Penulisan JSON lebih mudah dibaca dibandingkan dengan XML. Penulisan JSON dituliskan seperti sebuah dictionary yang memiliki key yang bersesuaian dengan sebuah value (data). 

XML: Bahasa markup yang dibuat untuk membawa data bukan menampilkan data. Penulisan XML diperuntukkan untuk dibaca oleh manusia dan mesin, meskipun tidak semudah membaca JSON dengan bahasa manusia. Penulisan XML mirip dengan HTML menggunakan tag pembuka dan tag penutup.

Beberapa perbedaan lain antara JSON dan XML adalah:
JSON:
    - mendukung tipe data berupa teks dan angka
    - Data siap diakses sebagai objek JSON
    - mudah mengakses value (karena seperti dictionary)
    - lebih mudah dibaca oleh manusia
    - didukung oleh hampir semua browser
    - tidak dapat melakukan pemrosesan objek

XML:
    - mendukung tipe data yang bervariasi, seperti gambar, dll
    - data pada XML harus diparse terlebih dahulu
    - mengakses value lebih sulit
    - lebih sulit dibaca oleh manusia
    - terdapat browser yang tidak mendukung html
    - dapat melakukan pemrosesan objek

2. Apakah perbedaan antara HTML dan XML?
HTML: HTML merupakan bahasa pemrograman yang berfungsi untuk menampilkan data dan mendefinisikan struktur dari sebuah tampilan web. Sama seperti XML, penulisan HTML juga menggunakan tag-tag yang membedakan masing-masing elemen HTML. 

HTML dan XML saling berhubungan. HTML berfungsi untuk menampilkan data, sedangkan XML berfungsi untuk menyimpan dan melakukan transfer data. XML dibuat karena HTML yang lebih dahulu muncul tidak memperbolehkan adanya definisi dari teks elemen baru (tidak extensible). 

HTML:
    - tidak case sensitive
    - tag sudah sesuai syntax 
    - closing tag tidak terlalu berpengaruh
    - memiliki fungsi utama untuk menampilkan data

XML:
    - case sensitive
    - tag dimodifikasi sesuai kebutuhan (karena biasanya berupa data yang disimpan)
    - closing tag sangat diperlukan
    - memiliki fungsi utama untuk menyimpan dan mentransfer data


Referensi:
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://www.guru99.com/json-vs-xml-difference.html
https://www.upgrad.com/blog/html-vs-xml/
https://www.britannica.com/technology/XML
https://www.guru99.com/xml-vs-html-difference.html
